package com.inspirethis.mike.viewpagerdemo;

import java.util.ArrayList;

/**
 * Created by mike on 2/18/16.
 */
public interface DogShowDBOperations {
    void updateDogShow(final String oldDogShowName, final DogShow updatedDogShow);
    DogShow getDogShow(final String dogShow);
    ArrayList<DogShow> getAllDogShows();
}
