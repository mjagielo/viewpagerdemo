 package com.inspirethis.mike.viewpagerdemo;

 import android.os.Bundle;
 import android.support.design.widget.FloatingActionButton;
 import android.support.design.widget.Snackbar;
 import android.support.v4.app.Fragment;
 import android.support.v4.app.FragmentManager;
 import android.support.v4.app.FragmentPagerAdapter;
 import android.support.v4.app.ListFragment;
 import android.support.v4.view.ViewPager;
 import android.support.v7.app.AppCompatActivity;
 import android.support.v7.widget.LinearLayoutManager;
 import android.support.v7.widget.RecyclerView;
 import android.support.v7.widget.Toolbar;
 import android.view.LayoutInflater;
 import android.view.Menu;
 import android.view.MenuItem;
 import android.view.View;
 import android.view.ViewGroup;
 import android.widget.SimpleCursorAdapter;

 import java.util.ArrayList;
 import java.util.LinkedHashMap;
 import java.util.List;
 import java.util.Map;

 public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
     private ArrayList<DogShow> mDogShowList;
     private ArrayList<Dog> mDogList;
     private ArrayList<String> mFragmentListTitles = null;
     private Map<String, ArrayList<Dog>> mDogMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDogMap = new LinkedHashMap<>();

        getDogShowData();
        setUpPagerAdapter(getFragments());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

     private void getDogShowData() {
         final DogAppDBAdapter database = DogAppDBAdapter.getDatabaseInstance(getApplicationContext());

         database.open();
         mDogList = database.getAllDogs();
         mDogShowList = database.getAllDogShows();
         database.close();

         for (Dog d : mDogList) {
             for (int i = 0; i < mDogShowList.size(); i++) {
                 if(d.getDogShowName().equals(mDogShowList.get(i))) {
                     mDogShowList.get(i).setDog(d);
                 }
             }
         }
     }


     private void setUpPagerAdapter(List<ListFragment> fragmentsList) {
         mSectionsPagerAdapter = new SectionsPagerAdapter(
                 getSupportFragmentManager(), fragmentsList);

         mViewPager = (ViewPager) findViewById(R.id.container);
         mViewPager.setOffscreenPageLimit(mDogShowList.size());
         mViewPager.setAdapter(mSectionsPagerAdapter);
     }

     // instantiate ListFragments for each dog list
     private ArrayList getFragments() {
         ArrayList<PlaceholderFragment> fList = new ArrayList<>();
         mFragmentListTitles = new ArrayList<>();

         if (mDogShowList != null) {
             for (int i = 0; i < mDogShowList.size(); i++) {
                 mFragmentListTitles.add(mDogShowList.get(i).getName());
                 if (mDogShowList.get(i).getDogs() != null) {
                     fList.add(PlaceholderFragment.newInstance(i, mDogShowList.get(i).getDogs()));
                 }
             }
         }
         return fList;
     }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private RecyclerView mRecyclerView;
        private SimpleCursorAdapter cursorAdapter;
        private DogListAdapter mAdapter;
        private List<Dog> mDogList;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber, ArrayList<Dog> list) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putParcelableArrayList("dog_list", list);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);

            ArrayList<Dog> dogList = getArguments().getParcelableArrayList("dog_list");

            mAdapter = new DogListAdapter(getContext(), dogList);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mAdapter);

            return rootView;
        }
    }
     // SectionsPagerAdapter extends FragmentPagerAdapter, returns a fragment corresponding to
     // each of the sections / tabs / pages.
     public class SectionsPagerAdapter extends FragmentPagerAdapter {
         private List<ListFragment> fragments;

         public SectionsPagerAdapter(FragmentManager fm, List<ListFragment> fragments) {
             super(fm);
             this.fragments = fragments;
         }

         @Override
         public Fragment getItem(int position) {
             return this.fragments.get(position);
         }

         @Override
         public int getCount() {
             return this.fragments.size();
         }

         @Override
         public CharSequence getPageTitle(int position) {
             return mFragmentListTitles.get(position);
         }
     }
}
