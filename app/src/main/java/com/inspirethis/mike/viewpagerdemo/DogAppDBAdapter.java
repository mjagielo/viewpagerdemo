package com.inspirethis.mike.viewpagerdemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by mike on 2/17/16.
 */
public class DogAppDBAdapter implements DogDBOperations, DogShowDBOperations {

    public static final String TAG = "DogAppDBAdapter";
    public static final String DOG_ROW_ID = "_id";
    public static final String DOG_BREED = "DogBreed";
    public static final String DOG_NAME = "DogName";
    public static final String DOG_AGE = "DogAge";
    public static final String DOG_LOCATION = "DogLocation";
    public static final String DOG_OWNER = "DogOwner";
    public static final String DOG_AWARD = "DogAward";
    public static final String DOG_ID = "DogID";

    public static final String DOG_SHOW = "DogShow";
    public static final String DOGSHOW_ROW_ID = "_id";
    public static final String DOGSHOW_NAME = "DogShowName";
    public static final String DOG_DOGSHOW_NAME = "DogDogShowName";

    public static final String DATABASE_NAME = "DogApplicationDB";
    private static final String DOG_T = "Dogs";
    private static final String DOGSHOW_T = "DogShows";
    private static final int DATABASE_VERSION = 1;

    public static DogAppDBAdapter databaseInstance;

    private MyDBHelper databaseHelper;

    private final Context context;

    private SQLiteDatabase database;

    public  static final Object[] dbLock = new Object[0];

    public static DogAppDBAdapter getDatabaseInstance(final Context context) {
        if (databaseInstance == null) {
            databaseInstance = new DogAppDBAdapter(context);
        }
        return databaseInstance;
    }

    private DogAppDBAdapter(Context context) {
        this.context = context;
        databaseHelper = new MyDBHelper(context);
    }

    public DogAppDBAdapter open() {
        databaseHelper = new MyDBHelper(context);
        try {
            database = databaseHelper.getWritableDatabase();
            Log.i("", " creating database: ");
        }catch(SQLiteException e) {
            Log.d("", "exception in creating database: " + e);
            // database = databaseHelper.getReadableDatabase();
        }
        return this;
    }

    public void close() {
        databaseHelper.close();;
    }

    @Override
    public long addDog(Dog dog) {

        ContentValues bucket = new ContentValues();
        bucket.put(DOG_BREED, dog.getBreed());
        bucket.put(DOG_NAME, dog.getName());
        bucket.put(DOG_AGE, dog.getAge());
        bucket.put(DOG_LOCATION, dog.getLocation());
        bucket.put(DOG_OWNER, dog.getOwner());
        bucket.put(DOG_DOGSHOW_NAME, dog.getDogShowName());


        return database.insert(DOG_T, null, bucket);
    }

    @Override
    public void updateDogShow(final String oldDogShowName, final DogShow updatedDogShow) {
        //Updates a specific tuple of DogShow Table
        ContentValues updatedValues = new ContentValues();
        updatedValues.put(DOGSHOW_NAME, updatedDogShow.getName());
        String whereClause = DOGSHOW_NAME + " = ?";
        String[] whereArgs = new String[]{oldDogShowName};
        database.update(DOGSHOW_T, updatedValues, whereClause, whereArgs);
    }

    @Override
    public void updateDog(int id, Dog updatedDog) {
        //modifyDog(id, updatedDog);
        //Updates a specific tuple of Dog Table
        ContentValues updatedValues = new ContentValues();
        updatedValues.put(DOG_BREED, updatedDog.getBreed());
        updatedValues.put(DOG_NAME, updatedDog.getName());
        updatedValues.put(DOG_AGE, updatedDog.getAge());
        updatedValues.put(DOG_LOCATION, updatedDog.getLocation());
        updatedValues.put(DOG_OWNER, updatedDog.getOwner());
        updatedValues.put(DOG_DOGSHOW_NAME, updatedDog.getDogShowName());
        String whereClause = DOG_ID + " = ?";
        String[] whereArgs = new String[]{Integer.toString(id)};

        database.update(DOG_T, updatedValues, whereClause, whereArgs);
    }


    @Override
    public void deleteDog(Dog dog) {
        String dogWhereClause = DOG_ID + " = ?";
        String[] dogWhereArgs = new String[1];
        database.delete(DOG_T, dogWhereClause, dogWhereArgs);
    }

    @Override
    public ArrayList<DogShow> getAllDogShows() {
        ArrayList<DogShow> dogShows = new ArrayList<DogShow>();
        Cursor dogShowCursor = database.query(DOGSHOW_T, null, null, null, null, null, null);
        DogShow dogShow;
        for (dogShowCursor.moveToFirst(); !dogShowCursor.isAfterLast(); dogShowCursor.moveToNext()) {
            dogShow = new DogShow(dogShowCursor.getString(dogShowCursor.getColumnIndexOrThrow(DOGSHOW_NAME)));
            //Build the Goals list within category
            for (Dog dog : extractDogs(dogShow.getName())) {
                dogShow.setDog(dog);
            }
            dogShows.add(dogShow);
        }
        return dogShows;
    }

    @Override
    public DogShow getDogShow(final String dogShowName) {
        DogShow dogShow = null;
        String selection = DOGSHOW_NAME + " = ?";
        String[] selectionArgs = new String[]{dogShowName};
        Cursor dogShowCursor = database.query(DOGSHOW_T, null, selection, selectionArgs, null, null, null);
        if (dogShowCursor.moveToFirst()) {
            dogShow = new DogShow(dogShowCursor.getString(dogShowCursor.getColumnIndexOrThrow(DOGSHOW_NAME)));

            for (Dog dog : extractDogs(dogShow.getName())) {
                dogShow.setDog(dog);
            }
        }
        return dogShow;
    }

    @Override
    public Dog getDog(int id) {
        String goalSelection = DOG_ID + " = ?";
        String[] dogSelectionArgs = new String[]{Integer.toString(id)};
        Cursor dogCursor = database.query(DOG_T, null, goalSelection, dogSelectionArgs, null, null, null);
        Dog extractedDog = null;
        if (dogCursor.moveToFirst()) {
            extractedDog = new Dog(dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_DOGSHOW_NAME)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_BREED)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_NAME)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_OWNER)),
                    dogCursor.getInt(dogCursor.getColumnIndexOrThrow(DOG_AGE)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_LOCATION)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_AWARD)));
        }

        return extractedDog;
    }


    @Override
    public ArrayList<Dog> getAllDogs() {
        ArrayList<Dog> dogs = new ArrayList<Dog>();
        Cursor dogCursor = database.query(DOG_T, null, null, null, null, null, null);
        Dog extractedDog = null;

        if (dogCursor.moveToFirst()) {
            for (dogCursor.moveToFirst(); !dogCursor.isAfterLast(); dogCursor.moveToNext()) {
                extractedDog = new Dog(dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_DOGSHOW_NAME)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_BREED)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_NAME)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_OWNER)),
                        dogCursor.getInt(dogCursor.getColumnIndexOrThrow(DOG_AGE)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_LOCATION)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_AWARD)));
                dogs.add(extractedDog);
            }
        }
        return dogs;
    }

    @Override
    public ArrayList<Dog> getDogs(final String dogShow) {
        return extractDogs(dogShow);
    }

    private ArrayList<Dog> extractDogs(String dogShow) {
        ArrayList<Dog> dogs = new ArrayList<Dog>();

        String dogSelection = DOG_DOGSHOW_NAME + " = ?";
        String[] dogSelectionArgs = new String[]{dogShow};
        Cursor dogCursor = database.query(DOG_T, null, dogSelection, dogSelectionArgs, null, null, null);

        Dog extractedDog = null;

        if (dogCursor.moveToFirst()) {
            for (dogCursor.moveToFirst(); !dogCursor.isAfterLast(); dogCursor.moveToNext()) {
                extractedDog = new Dog(dogShow,
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_BREED)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_NAME)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_OWNER)),
                        dogCursor.getInt(dogCursor.getColumnIndexOrThrow(DOG_AGE)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_LOCATION)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_AWARD)));

                dogs.add(extractedDog);
            }
        }
        return dogs;
    }

    private static class MyDBHelper extends SQLiteOpenHelper {
        public MyDBHelper(final Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE " + DOGSHOW_T + " (" +
                            DOGSHOW_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            DOGSHOW_NAME + " TEXT NOT NULL UNIQUE);"
            );

            db.execSQL("CREATE TABLE " + DOG_T + " (" +
                            DOG_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            DOG_DOGSHOW_NAME + " TEXT NOT NULL, " +
                            DOG_BREED + " TEXT NOT NULL, " +
                            DOG_NAME + " TEXT NOT NULL, " +
                            DOG_OWNER + " TEXT NOT NULL, " +
                            DOG_AGE + " INTEGER, " +
                            DOG_LOCATION + " TEXT NOT NULL, " +
                            DOG_AWARD + " TEXT NOT NULL);"
            );

            buildDummyDogShows(db);
            buildDummyData(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DOGSHOW_T);
            db.execSQL("DROP TABLE IF EXISTS " + DOG_T);
            onCreate(db);
        }

        private void buildDummyData(SQLiteDatabase db) {
            Log.d("", "in build dummy data: ......");
            ContentValues bucket = new ContentValues();
            bucket.put(DOG_DOGSHOW_NAME, "Westminster" );
            bucket.put(DOG_BREED, "Labrador");
            bucket.put(DOG_NAME, "Max");
            bucket.put(DOG_OWNER, "Bart Simpson");
            bucket.put(DOG_AGE, 5);
            bucket.put(DOG_LOCATION, "New Zealand");
            bucket.put(DOG_AWARD, "Gold Medal");
            db.insert(DOG_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOG_DOGSHOW_NAME, "Westminster" );
            bucket.put(DOG_BREED, "German Shepard");
            bucket.put(DOG_NAME, "Duke");
            bucket.put(DOG_OWNER, "Sally Jones");
            bucket.put(DOG_AGE, 5);
            bucket.put(DOG_LOCATION, "New Brunswick");
            bucket.put(DOG_AWARD, "Bronze Medal");
            db.insert(DOG_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOG_DOGSHOW_NAME, "Westminster" );
            bucket.put(DOG_BREED, "Irish Setter");
            bucket.put(DOG_NAME, "Red");
            bucket.put(DOG_OWNER, "Cleve Borderdash");
            bucket.put(DOG_AGE, 5);
            bucket.put(DOG_LOCATION, "New Jersey");
            bucket.put(DOG_AWARD, "Silver Medal");
            db.insert(DOG_T, null, bucket);


            bucket = new ContentValues();
            bucket.put(DOG_DOGSHOW_NAME, "Upper Darby" );
            bucket.put(DOG_BREED, "Schnauzer");
            bucket.put(DOG_NAME, "Fritz");
            bucket.put(DOG_OWNER, "Colleen Burke");
            bucket.put(DOG_AGE, 3);
            bucket.put(DOG_LOCATION, "Scottsdale");
            bucket.put(DOG_AWARD, "Gold Medal");
            db.insert(DOG_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOG_DOGSHOW_NAME, "Upper Darby" );
            bucket.put(DOG_BREED, "Irish Wolfe Hound");
            bucket.put(DOG_NAME, "Alf");
            bucket.put(DOG_OWNER, "Fred HoundSitter");
            bucket.put(DOG_AGE, 7);
            bucket.put(DOG_LOCATION, "Willoughsby");
            bucket.put(DOG_AWARD, "Bronze Medal");
            db.insert(DOG_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOG_DOGSHOW_NAME, "Upper Darby" );
            bucket.put(DOG_BREED, "Bull Dog");
            bucket.put(DOG_NAME, "Dozer");
            bucket.put(DOG_OWNER, "Cleve Borderdash");
            bucket.put(DOG_AGE, 3);
            bucket.put(DOG_LOCATION, "New Jersey");
            bucket.put(DOG_AWARD, "Gold Medal");
            db.insert(DOG_T, null, bucket);

        }

        private void buildDummyDogShows(SQLiteDatabase db) {

            String[] showNames = {"Westminster", "Upper Darby", "Yorkshire"};

            ContentValues bucket = new ContentValues();
            bucket.put(DOGSHOW_NAME, showNames[0]);
            db.insert(DOGSHOW_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOGSHOW_NAME, showNames[1]);
            db.insert(DOGSHOW_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOGSHOW_NAME, showNames[2]);
            db.insert(DOGSHOW_T, null, bucket);

        }
    }

}