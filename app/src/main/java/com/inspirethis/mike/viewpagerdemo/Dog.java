package com.inspirethis.mike.viewpagerdemo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by mike on 2/17/16.
 */
public class Dog implements Parcelable{

    public static int next_id = 1;
    private HashMap<String, String> map = new HashMap<String, String>();

    private String breed;
    private String name;
    private String owner;
    private int age;
    private String location;
    private String award;
    private String dogShowName;

    public Dog(Parcel in ) {
        readFromParcel( in );
    }

    public Dog (String breed, String name, String owner, int age, String location, String award) {
        this.breed = breed;
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.location = location;
        this.award = award;
    }

    public Dog (String dogShowName, String breed, String name, String owner, int age, String location, String award) {
        this.dogShowName = dogShowName;
        this.breed = breed;
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.location = location;
        this.award = award;

    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setAward(String award) {this.award = award; }

    public String getAward() {
        return award;
    }

    public String getDogShowName() {
        return dogShowName;
    }

    public void setDogShowName(String dogShowName) {
        this.dogShowName = dogShowName;
    }

    public static final Creator CREATOR = new Creator() {
        public Dog createFromParcel(Parcel in ) {
            return new Dog( in );
        }

        public Dog[] newArray(int size) {
            return new Dog[size];
        }
    };


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(map.size());
        for (String key : map.keySet()) {
            dest.writeString(key);
            dest.writeString(map.get(key));
        }
        dest.writeString(dogShowName);
        dest.writeString(breed);
        dest.writeString(name);
        dest.writeString(owner);
        dest.writeInt(age);
        dest.writeString(location);
        dest.writeString(award);
    }

    private void readFromParcel(Parcel in ) {
        int size = in.readInt();
        for(int i = 0; i < size; i++) {
            String key = in.readString();
            String value = in.readString();
            map.put(key, value);
        }
        dogShowName = in.readString();
        breed = in.readString();
        name = in.readString();
        owner = in.readString();
        age = in.readInt();
        location = in.readString();
        award = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
