package com.inspirethis.mike.viewpagerdemo;

import java.util.ArrayList;

/**
 * Created by mike on 2/17/16.
 */
public interface DogDBOperations {
    long addDog(Dog dog);
    void updateDog(final int id, final Dog updatedDog);
    void deleteDog(final Dog dog);
    Dog getDog(final int id);
    ArrayList<Dog> getAllDogs();
    ArrayList<Dog> getDogs(final String dogShow);
}
