package com.inspirethis.mike.viewpagerdemo;

import java.util.ArrayList;

/**
 * Created by mike on 2/17/16.
 */
public class DogShow {
    private String name;
    private ArrayList<Dog> dogs;

    public DogShow(final String name) {
        this.name = name;
        dogs = new ArrayList<Dog>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Dog> getDogs() {
        return dogs;
    }

    public void setDog(final Dog dog) {
        dogs.add(dog);
    }

    public boolean removeDog(final Dog dog) {
        for (int i = 0; i < dogs.size(); i++) {
            if (dogs.get(i).equals(dog)) {
                dogs.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof DogShow))
            return false;
        return ((DogShow) o).getName().equals(this.getName());
    }
}
