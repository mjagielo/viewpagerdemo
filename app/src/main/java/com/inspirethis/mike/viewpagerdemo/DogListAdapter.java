package com.inspirethis.mike.viewpagerdemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mike on 2/17/16.
 */
public class DogListAdapter extends RecyclerView.Adapter<DogListAdapter.ViewHolder> {

    Context mContext;
    OnItemClickListener mItemClickListener;
    //protected List<Dog> mData = new ArrayList<>();
    private List<Dog> mDogList;


    public DogListAdapter(Context context, List<Dog> list) {
        this.mContext = context;
        this.mDogList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_places, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Dog dog = mDogList.get(position);
        holder.dogName.setText(dog.getName());
        holder.dogBreed.setText(dog.getBreed());
        holder.dogOwner.setText(dog.getOwner());
        holder.dogLocation.setText(dog.getLocation());
        holder.dogAward.setText(dog.getAward());
    }

    @Override
    public int getItemCount() {
        //Log.d("","************ in getItemCount, size: " + mDogList.size());

        return mDogList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout dogHolder;
        public RelativeLayout dogNameHolder;
        public TextView dogName;
        public TextView dogBreed;
        public TextView dogOwner;
        public TextView dogLocation;
        public TextView dogAward;
        //public ImageView dogImage;

        public ViewHolder(View itemView) {
            super(itemView);
            dogHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            dogNameHolder = (RelativeLayout) itemView.findViewById(R.id.dogNameHolder);
            dogName = (TextView) itemView.findViewById(R.id.dogName);
            dogBreed = (TextView) itemView.findViewById(R.id.dogBreed);
            dogOwner = (TextView) itemView.findViewById(R.id.dogOwner);
            dogLocation = (TextView) itemView.findViewById(R.id.dogLocation);
            dogAward = (TextView) itemView.findViewById(R.id.dogAward);

            //dogImage = (ImageView) itemView.findViewById(R.id.dogImage);
            //dogHolder.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(itemView, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
